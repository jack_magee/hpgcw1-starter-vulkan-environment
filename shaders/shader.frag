#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fragColor; #take an input of the colours from the vertex shader

layout(location = 0) out vec4 outColor;

void main() {
    outColor = vec4(fragColor, 1.0); #output will be automatically interpolated and then the output colour will be given for each fragment
}

