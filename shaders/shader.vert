#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec3 fragColor;

vec2 positions[3] = vec2[]( #positions of the vertiecs we wish to display on screen
    vec2(0.0, -0.5),
    vec2(0.5, 0.5),
    vec2(-0.5, 0.5)
);

vec3 colors[3] = vec3[]( #the respective colours of the vertices
    vec3(1.0, 0.0, 0.0),
    vec3(0.0, 1.0, 0.0),
    vec3(0.0, 0.0, 0.4)
);

void main() {
    gl_Position = vec4(positions[gl_VertexIndex], 0.0, 1.0); #gl_Position is the standard output for the gl_VertexIndex, a hardcoded array of positions
    fragColor = colors[gl_VertexIndex]; #output the colour to the fragment shader
}

