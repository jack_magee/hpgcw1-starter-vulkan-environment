#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <algorithm>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <cstdint>
#include <optional>
#include <set>

/*define width and heights as constants so can be used throughout the file*/
const int WIDTH = 800;
const int HEIGHT = 600;

/*define the number of framems that can be processed concurrently*/
const int MAX_FRAMES_IN_FLIGHT = 2;

/*define the validation layers to be the standard layer included in sdk. As vulkan is designed to be as performant
as posible, having validation build in would be costly, but as everything needs to be manually set to get it
working, then easy mistakes can be made and not picked up. So having validation layers at least means we can
enable checking when developing, and then in release have very little overhead.*/
const std::vector<const char*> validationLayers = {
	"VK_LAYER_KHRONOS_validation"
};

const std::vector<const char*> deviceExtensions = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

/*configuration variables to specify the layers to enable, leaves it up to the programmer whether to include for debug*/
#ifdef NDEBUG
const bool enableValidationLayers = false; //if not in debug mode, then disable
#else
const bool enableValidationLayers = true; //if in debug mode then enable the validation layers
#endif

/*Function to create the debug object, needs to be a global function as the extension it is calling is not
directly loaded, so the address of it needs to be manually looked up*/
VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) {
	//function call to get the address of the function, if it cannot be found then it will return a nullptr
	auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
	if (func != nullptr) { //if the function has been found then return the function
		return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
	}
	else {
		return VK_ERROR_EXTENSION_NOT_PRESENT; //if not then throw an error.
	}
}

/*As the object is initialized manually, then also need to mamually clean it up*/
void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator) {
	//needto see if the function exists and if it does then pass the function back as the return value so that it can be destroyed
	auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
	if (func != nullptr) {
		func(instance, debugMessenger, pAllocator);
	}
}

/*Almost every operation in vulkan requires it to be passed to a queue to be processed. We need different
queue families so that each familiy only allows a certain amount of commands. Here we bundle all the families
into a struct so that the code is simplified*/
struct QueueFamilyIndices {
	std::optional<uint32_t> graphicsFamily; //to store graphics commands
	std::optional<uint32_t> presentFamily; //make sure card can disply to the screen

	bool isComplete() { //to make sure that the device is completely useable
		return graphicsFamily.has_value() && presentFamily.has_value();
	}
};

/*this is needed so that data can be passed around in a struct about the swap chain*/
struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilities; //what are the capabilities of the swap chain? min/max things in chain, max width and height
	std::vector<VkSurfaceFormatKHR> formats; //the surface formats such as pixel format and colour space
	std::vector<VkPresentModeKHR> presentModes;	//and the available presentation modes
};

/*The main class for the application, it stores all the vulkan functions needed
as well as ititializations*/
class HelloTriangleApplication {
public:
	void run() {		//function called to run the entire application and call the appropriate inits
		initWindow();	//do you want to use a window to display cool things? do that here. This inits GLFW and creates a window
		initVulkan();	//this will call all the vulkan objects and initialise them, its job it to prepare everything
		mainLoop();		//This function starts a loop that will be run until the window is closed. All calls to manipulate things in rendering are here
		cleanup();		//This function will deallocate all the resources that have been used properly
	}

private:
	GLFWwindow* window; //Add a GLFW window to the class so we can display a window on the screen

	VkInstance instance;//Add a vulkan instance to the class so that we can use the vulkan library
	VkDebugUtilsMessengerEXT debugMessenger; //hanbdle to the debug message
	VkSurfaceKHR surface; //This is needed so that we are able to display anything
	
	/*this will store the graphics cards we select, and it will be destroyed when the vulkan instance is destoyed
	so no clean up is necessary*/
	VkPhysicalDevice physicalDevice = VK_NULL_HANDLE; //physical device
	VkDevice device; //logical device

	VkQueue graphicsQueue; //need a handle to interact with the graphics queue
	VkQueue presentQueue;	//need a handle to interact with the dispay queue

	VkSwapchainKHR swapChain; //need a handle to interact with the swap chain
	std::vector<VkImage> swapChainImages; //this stores the images in the swap chain so they can be displayed
	VkFormat swapChainImageFormat; //handle to the stored format of the images in the swapchain
	VkExtent2D swapChainExtent; //handle to the img size stored in the swap chain
	std::vector<VkImageView> swapChainImageViews; //vector to store the image views from the swap chain
	std::vector<VkFramebuffer> swapChainFramebuffers;

	VkRenderPass renderPass;	//a member to hold the renderpass object
	VkPipelineLayout pipelineLayout; //member to store how the pipeline is layed out
	VkPipeline graphicsPipeline;	//member to store the specifics of the graphics pipeline

	VkCommandPool commandPool; //define the command pool for storing commands to be passed to the buffer
	std::vector<VkCommandBuffer> commandBuffers; //vector to store command buffers for all images in the swapchain

	std::vector<VkSemaphore> imageAvailableSemaphores; //tells that the image has been aqauired and is ready to be rendered
	std::vector<VkSemaphore> renderFinishedSemaphores; //tells that the image has finished rendering
	std::vector<VkFence> inFlightFences; //use fences to enable cpu-gpu syncronisation, syncs frames in flight between cpu-gpu
	std::vector<VkFence> imagesInFlight; //we need to track	for each image in the swap chain if a frame in flight is using it
	size_t currentFrame = 0; //we start on the 1st frame

	/*As we have chosen to use GLFW, need a function to handle initializing
	GLFW and creating a window. By using GLFW it will use its own definitions
	and load the Vulkan header with it.*/
	void initWindow() {
		glfwInit(); //this initialises the GLFW library

		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); //As using vulkan and not openGL, need to tell it not to create openGL context

		/*This creates the actual window that will be displayed on screen.
		The window is declared above so this references it and initialises it
		WIDTH - The width of the window (arg1)
		HEIGHT - The height of the window (arg2)
		"Vulkan" - The title displayed for the window (arg3)
		nullptr - The monitor to display on, nullptr refers to primary if nothing is set (arg4)
		nullptr - ONLY USED FOR OPENGL!*/
		window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan", nullptr, nullptr);
		
		glfwSetWindowUserPointer(window, this);
	}

	/*Function to initiailise the Vulkan library as well as all the functions for
	the pipeline and rendering*/
	void initVulkan() {
		createInstance();		//Function call to create a vulkan instance and fill in some information about the application
		setupDebugMessenger(); 	//Function to enable debug messages as handle must be explicitly initialised and destroyed
		createSurface();		//fuinction to create a surface so that things can be displayed on the screen
		pickPhysicalDevice();	//function to choose the graphics card we will use
		createLogicalDevice();	//function to create a logical device to be used in the instance
		createSwapChain();		//function to create the swapchain so images can be stored ready to display
		createImageViews();		//fuction to convert the images in te buffer to images thhat are able to be displayed on the screen
		createRenderPass();		//function to set up all the render path details
		createGraphicsPipeline(); //function to generaate the graphics pipeline for the application
		createFramebuffers();	//function to create the framebuffers that are used to store attacthments created in the render pass
		createCommandPool();	//function to create the command pool that will manage the memory used to store commands
		createCommandBuffers();	//function to create the command buffers to store all the operations that will need to be done
		createSyncObjects();	//function to create all the sncy objects
	}

	/*This is the main loop that is kept running whilst the window is open, it polls
	any inputs to the window and then calls drawFrame which deals with displaying
	anything on the screen*/
	void mainLoop() {

		/*Loop to keep the application running until and error occurs or the window 
		is closed*/
		while (!glfwWindowShouldClose(window)) { //is everything running smoothly and is the window still open?
			glfwPollEvents(); //If so then we need to keep checking for events such as resizing, minimizing or closing
			drawFrame();		//every loop we need to draw the frame, we do that here
		}

		vkDeviceWaitIdle(device); //wait for the logical device to finish operations before closing the window so that things dont crash
	}

	/*Function for cleaning up everything to do wth the swap chain in the appropriate order, used to make sure all done in the correct order*/
	void cleanupSwapChain() {
		//for all of the framebuffers in the swap chain, they need to be destroyed individually
		for (auto framebuffer : swapChainFramebuffers) {
			vkDestroyFramebuffer(device, framebuffer, nullptr);
		}

		//we can now destroy the command buffers as there is no mroe data to execute
		vkFreeCommandBuffers(device, commandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());

		vkDestroyPipeline(device, graphicsPipeline, nullptr); //as its being used throughout the probgram, needs to be destroyed at the end
		vkDestroyPipelineLayout(device, pipelineLayout, nullptr); //the pipeline needs to be destroyed at the end as it will be displaying things untuk the end
		vkDestroyRenderPass(device, renderPass, nullptr);

		for (auto imageView : swapChainImageViews) { //before the program closes, need to destroy each imageview in the swap chain
			vkDestroyImageView(device, imageView, nullptr);
		}
		//once all image views have been deleted, can destory the swap chain object
		vkDestroySwapchainKHR(device, swapChain, nullptr);
	}

	/*Everything in valkan needs to be manually set up, and then manually cleaned up. The clean up is all 
	done here so that the code is tidy and that it is done in the correct order*/
	void cleanup() {
		cleanupSwapChain(); //as the swpchain was explicitly created, then needs to be cleaned, but must be done before anything else so to not crash

		//for all the frames that are crrently in use, need to destroy all the semaphores for them when we are clearning everything else
		for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
			vkDestroySemaphore(device, renderFinishedSemaphores[i], nullptr);
			vkDestroySemaphore(device, imageAvailableSemaphores[i], nullptr);
			vkDestroyFence(device, inFlightFences[i], nullptr);
		}

		//once swapchain has been cleaned up, then can destroy command pool as it wont be used anymore
		vkDestroyCommandPool(device, commandPool, nullptr);

		vkDestroyDevice(device, nullptr); //destroy the logical device, no need to supply an instance as they do not directy interact

		if (enableValidationLayers) { //if we are using validation layers then we need to manually clear up the debug object
			DestroyDebugUtilsMessengerEXT(instance, debugMessenger, nullptr); //need to find the debugMessanger and then destroy it
		}

		vkDestroySurfaceKHR(instance, surface, nullptr); //need to destroy the surface object in the instance, must be done befor the instance

		/*The vulkan instance should only be destroyed just before the program ends, so it is included in the 
		cleanup function. The vulkan library has a destructor built in so need only call the destructor with
		the required instance to destroy
		instance - the vulkan instance to be destroyed
		nullptr - As no allocation callbacks were provided in the initialization then leave as null*/
		vkDestroyInstance(instance, nullptr);	

		/*When the application is closed then we need to destroy the window and then
		close the GLFW library that had been previously initialised*/
		glfwDestroyWindow(window); 
		glfwTerminate();
	}

	/*To create an instance we need to fill in a struct with appropriate info
	about our application. We do this here. It is optional but useful for 
	optimisation.*/
	void createInstance() {
		/*First we need to check to see if we are in debug mode and have validation layers enabled and that
		they are all working. If they are not then throw an error*/
		if (enableValidationLayers && !checkValidationLayerSupport()) {
			throw std::runtime_error("validation layers requested, but not available!");
		}

		VkApplicationInfo appInfo = {}; //Struct to fill in with appropriate information for the application to enable to driver to optimise
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO; 	//Define the type of the structure, here we are storing the application info
		appInfo.pApplicationName = "Hello Triangle"; 			//The name of the application
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0); 	//This is the current version of the program as supplied by the develeoper, may assist in debug
		appInfo.pEngineName = "No Engine"; 						//Name of the engine used, here we are not using an engine so no name needed
		appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0); 		//Similar to above, developer supplied version number of the engine used
		appInfo.apiVersion = VK_API_VERSION_1_0; 				//API version that the application is using. Can also be set by checking what version device supports

		/*More information is required by vulkan to tell it which global and validation layers we wish to use.
		THIS STRUCT IS NOT OPTIONAL. MAKE SURE IT ALWAYS IS INCLUDED!*/
		VkInstanceCreateInfo createInfo = {}; 					//Initialise the struct to be filled in with creation information
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO; //This strucure is destined to store the instance creation information
		createInfo.pApplicationInfo = &appInfo; 				//The application info that we are using is simply the defined information above

		auto extensions = getRequiredExtensions(); 				//get all the extensions that are being used by the application
		createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size()); //Store how many extensions are being used
		createInfo.ppEnabledExtensionNames = extensions.data();	//as well as all of their names

		VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
		/*If the validation layers are enabled then need to tell the instance before it is initialized*/
		if (enableValidationLayers) {
			createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size()); //Define the numbr of layers being enabled
			createInfo.ppEnabledLayerNames = validationLayers.data(); //As well as all the layers named. This data was worked out when layers were enabled.

			populateDebugMessengerCreateInfo(debugCreateInfo);
			createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*)&debugCreateInfo;
		}
		else {
			createInfo.enabledLayerCount = 0; 	//If there are no additional layers, then set to 0
			createInfo.pNext = nullptr;			
		}

		/*Now everything is specified, it is time to create the instance of Vulkan. This is done with the
		vkCreateInstance call. The implementation of this function means that is returns a vkResult type
		so that can very easily check to see if the initiation was a success*/
		if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS) {
			//If there are any errors, then an error message will be thrown and caught by the upper level so that it can be displayed in the terminal
			throw std::runtime_error("failed to create instance!");
		}
	}

	/*Function to fill in the debug message structure*/
	void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo) {
		createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT; //The type of the structure, here used to store debug messages
		createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | 
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | 
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT; //the message severity, this indicates all the severities that it would be called for
		createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
		 	VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | 
			VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT; //the messages that would it would be called for
		createInfo.pfnUserCallback = debugCallback; //pointer to the callback function
	}

	/*function to set up the debug message if in debug mode. Must be explicitly set up by the programmer*/
	void setupDebugMessenger() { //if were not debugging then there is no use for us to enable it. It is wasted resources
		if (!enableValidationLayers) return;

		VkDebugUtilsMessengerCreateInfoEXT createInfo; //initialise the structure
		populateDebugMessengerCreateInfo(createInfo); //fill in the structure with all appropriate info, done in a function call

		/*function call to check if the we can create the extension object, if it is not available then
		throw an error.*/
		if (CreateDebugUtilsMessengerEXT(instance, &createInfo, nullptr, &debugMessenger) != VK_SUCCESS) {
			throw std::runtime_error("failed to set up debug messenger!");
		}
	}

	/*Function to set up the surface, using glfw means that it will work multiplatform.*/
	void createSurface() {
		//call the appropriate fiunction to create a window, and make sure it was successful
		if (glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS) {
			throw std::runtime_error("failed to create window surface!");
		}
	}

	/*Now that the vulkan instance has been set up, we need to find a suitable graphics card (or few) to use*/
	void pickPhysicalDevice() {
		uint32_t deviceCount = 0; //init the number of cards to be 0 incxase none are available
		vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);	//first need to enumberate the cards to seee how many we have

		if (deviceCount == 0) { //if there are no catrds then there is not much point in continuing
			throw std::runtime_error("failed to find GPUs with Vulkan support!"); //so throw an error saying no suitabel cards
		}

		std::vector<VkPhysicalDevice> devices(deviceCount); //allocate an array to store all the useable device handles
		vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());	//store all the cards

		for (const auto& device : devices) { //for each of the graphics cards, go through and check that it is suitable for what it is wanted for
			if (isDeviceSuitable(device)) {
				physicalDevice = device; //if a device is suitable, set that as the device to use
				break;
			}
		}

		if (physicalDevice == VK_NULL_HANDLE) { //if non are suitabel then throwan error
			throw std::runtime_error("failed to find a suitable GPU!");
		}
	}

	/*Function to create a logical device to use in the vulkan instance*/
	void createLogicalDevice() {
		//only need to find a device with graphisc capabilites
		QueueFamilyIndices indices = findQueueFamilies(physicalDevice);

		std::vector<VkDeviceQueueCreateInfo> queueCreateInfos; //vector storing the queue information
		std::set<uint32_t> uniqueQueueFamilies = { indices.graphicsFamily.value(), indices.presentFamily.value() };

		/*Herre we can use vulkans priority to specify the buffer execution*/
		float queuePriority = 1.0f;
		for (uint32_t queueFamily : uniqueQueueFamilies) { //for each of the families
			VkDeviceQueueCreateInfo queueCreateInfo = {}; //create a struct
			queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO; //and it is storing device information
			queueCreateInfo.queueFamilyIndex = queueFamily; //and the family tyope is the unique family type
			queueCreateInfo.queueCount = 1;	//Thjere is only 1 queue for each queue
			queueCreateInfo.pQueuePriorities = &queuePriority;	//and the priority is manually set to 1, this could be dynamically set deoending on use
			queueCreateInfos.push_back(queueCreateInfo); //add each struct to the device info vector
		}

		//now we need to specify the device features
		VkPhysicalDeviceFeatures deviceFeatures = {};

		VkDeviceCreateInfo createInfo = {}; //create a struct to store logical device features
		createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO; //its a device...

		createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size()); //the number of features is the same
		createInfo.pQueueCreateInfos = queueCreateInfos.data(); //and the data is the same

		createInfo.pEnabledFeatures = &deviceFeatures; // the only features we can use are those supported by the device

		createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size()); //the enable extensions is the same as the set enabled in the progeam, this makes sure we can use a swapchain
		createInfo.ppEnabledExtensionNames = deviceExtensions.data(); //and the data is the same

		if (enableValidationLayers) { //this is ignored in newer versions but best include anyway...
			createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size()); //tell how many validation layers are being used if debug
			createInfo.ppEnabledLayerNames = validationLayers.data();//and trhe data for the layers
		}
		else {
			createInfo.enabledLayerCount = 0; //else there are no validation layers
		}

		/*We can now initialise the logiacl device with a fnction call, adn then check to make sure that it
		was initialized without errors*/
		if (vkCreateDevice(physicalDevice, &createInfo, nullptr, &device) != VK_SUCCESS) {
			throw std::runtime_error("failed to create logical device!");
		}

		vkGetDeviceQueue(device, indices.graphicsFamily.value(), 0, &graphicsQueue);
		vkGetDeviceQueue(device, indices.presentFamily.value(), 0, &presentQueue);
	}

	/*function to create the swap chain from the information generated in the structs, allows us to set how many
	images can be queued waiting to be displayed*/
	void createSwapChain() {
		//first we need to extract the swapchain support information so that it can be used to set up the swap chain
		SwapChainSupportDetails swapChainSupport = querySwapChainSupport(physicalDevice);

		VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats); //what is the ideal surface format?
		VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes); //what is the ideal presentation mode
		VkExtent2D extent = chooseSwapExtent(swapChainSupport.capabilities); //and what is the ideal resolution

		uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1; //this makes sure there are always atleast 1 image in the swap chain so we dotn havw delays
		if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) { //check to see the max number of supported images
			imageCount = swapChainSupport.capabilities.maxImageCount; //if it is greater than the min+1, then use as the new number to store
		}

		//need to fill in a struct so we can pass information around
		VkSwapchainCreateInfoKHR createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR; //we are storing information about the swapchain
		createInfo.surface = surface; //the surface type we are using is the one defined in the application

		createInfo.minImageCount = imageCount; //the number of images we shall be storing
		createInfo.imageFormat = surfaceFormat.format; //the extracted imahe format
		createInfo.imageColorSpace = surfaceFormat.colorSpace; //and the colour space we will use
		createInfo.imageExtent = extent; //the extracted ideal resolution
		createInfo.imageArrayLayers = 1; //the number of layers each image consists of. usually 1 unless developing in VR or stereoscopic 3d
		createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;	//what will we be using the images for? here we can specify to load into the queue so we can perfomr ops on them
																		//we are using to directly render so used for colour attatchment

		QueueFamilyIndices indices = findQueueFamilies(physicalDevice); //find the queue families for the physical device used so we can sepecify how images will be used accross families
		uint32_t queueFamilyIndices[] = { indices.graphicsFamily.value(), indices.presentFamily.value() };

		if (indices.graphicsFamily != indices.presentFamily) { //see is the grapphics and display families are different
			createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT; //then images need to be used concurrently without explicit ownershp
			createInfo.queueFamilyIndexCount = 2; //share over both families
			createInfo.pQueueFamilyIndices = queueFamilyIndices;
		}
		else {
			createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE; //if they are the same then dont need to be shared and can be used exclusively
		}

		//do you want any transformation to happen on the swapchain? if not use the current transform
		createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
		createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR; //are you blending with the windowing system? if not set to be opaque
		createInfo.presentMode = presentMode; //this is the extracted presentation mode
		createInfo.clipped = VK_TRUE; //are we going care about the colour of pixels we cannot see? if not then set to true so they are clipped

		//now all details have been set for the swap chain, it is time to initialise it, if it fails then throw an error
		if (vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapChain) != VK_SUCCESS) {
			throw std::runtime_error("failed to create swap chain!");
		}

		//now we need to get the images to be displayed
		vkGetSwapchainImagesKHR(device, swapChain, &imageCount, nullptr); //how many are there?
		swapChainImages.resize(imageCount); //resize the number of images stored to be how many there are
		vkGetSwapchainImagesKHR(device, swapChain, &imageCount, swapChainImages.data()); //populate with the current stored images

		swapChainImageFormat = surfaceFormat.format; //the same format as used for the images
		swapChainExtent = extent; //and the same resolution
	}

	/*Function to create a basic image view for every image in the swap chain so they can be used as colour targets later on*/
	void createImageViews() {
		swapChainImageViews.resize(swapChainImages.size()); //resize the swapchain image views to be the same size as the number of images in the swap chain

		//need to create an image view for all the images in the swap chain by iterating over all of them
		for (size_t i = 0; i < swapChainImages.size(); i++) {
			VkImageViewCreateInfo createInfo = {}; //create the struct for each image in the swap chain so we can populate it with appropriate info
			createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO; //were storing image view data
			createInfo.image = swapChainImages[i]; //the image we want to work with is the corresponding image in the swap chain
			createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D; //we are treating the image as a 2D plane
			createInfo.format = swapChainImageFormat; //the image has the same format as the one in the swap chain
			createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY; //do we want to do anything fancy with the colour channels? if not then use default identity
			createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY; // "" 
			createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY; // ""
			createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY; // ""
			createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT; //the image is going to be used for displaying colour
			createInfo.subresourceRange.baseMipLevel = 0; //and it will use no mipmap levels
			createInfo.subresourceRange.levelCount = 1; //just a flat image so only using 1 lavel
			createInfo.subresourceRange.baseArrayLayer = 0; //theres no need to have a base layer for the image
			createInfo.subresourceRange.layerCount = 1; //the only layer is the image layer

			//using the built in function with the above struct details then we can make the image view from the swapchain image, if it fails then throw an error
			if (vkCreateImageView(device, &createInfo, nullptr, &swapChainImageViews[i]) != VK_SUCCESS) {
				throw std::runtime_error("failed to create image views!");
			}
		}
	}

	/*Before anything in the pipeline gets done, we need to tell vulkan about any of the framebuffer attachments we will be using
	in the application, this can be things such as how many colour and depth buffers there are and how many samples to use. 
	We store all this information in a render pass object. We shall create this here*/
	void createRenderPass() {
		//In this application we are only using a single colour buffer attachment, represented by a single image in the swap chain
		VkAttachmentDescription colorAttachment = {}; //create the struct
		colorAttachment.format = swapChainImageFormat;	//we are using the same image format as in the swap chain
		colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;//not using multisampling so only need 1 sample
		colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR; //what are we going to do with the data in the attachment? we shall clear it so we get a black frame
		colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE; //We want to see the image so store the data to use
		colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE; //we aren't using stencils so we dont care
		colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE; //" 	"
		colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED; //we aren't caring about the previous images layout, can be used to optimise textures
		colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;	//we want to displat the image so it needs to be presented

		/*each render pass may have multiple subpasses to perform operations and save memory, but for this application we are only going
		to be using a single subpass and single attachment. For each attachment we need to set them up. We do that here*/
		VkAttachmentReference colorAttachmentRef = {}; //set up attachment struct
		colorAttachmentRef.attachment = 0; //the index of the attachment we are referenciong
		colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;	//use the layout for best performance

		//Now we define the subpass
		VkSubpassDescription subpass = {}; //set up the struct
		subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS; //we will be using it in the graphics subpass (can also do compute subpasses)
		subpass.colorAttachmentCount = 1; //and how many attachments are we using?
		subpass.pColorAttachments = &colorAttachmentRef;	//they need to be referenced

		//the transition at the start of the render pass it not at the approprtate time, so can set up a dependency so we can make sure it doesn't
		//start until the imge is available
		VkSubpassDependency dependency = {}; //create the structto store the dependency
		dependency.srcSubpass = VK_SUBPASS_EXTERNAL; //we are refering to the first subpass
		dependency.dstSubpass = 0; //and there is only 1 so index is 0
		dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT; //which operations are we needing to wait on?
		dependency.srcAccessMask = 0; //which stage is it done?
		dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT; //the stages are in the colour attacthment stage
		dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT; //wait til read or written depending on the situation

		/*Now the attachments and the subpasses have been defined, can create a render pass with the appropritate information*/
		VkRenderPassCreateInfo renderPassInfo = {}; //struct to store the data
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;	//it is storing render pass information
		renderPassInfo.attachmentCount = 1;	//we only have 1 attachment
		renderPassInfo.pAttachments = &colorAttachment; //and its the previously defined colour attachment
		renderPassInfo.subpassCount = 1;	//and we only have 1 subpass
		renderPassInfo.pSubpasses = &subpass;	//that was defined earluer
		renderPassInfo.dependencyCount = 1; //how many dependancies are there
		renderPassInfo.pDependencies = &dependency; //and what are they?

		//now all the information about the render pass object has been filled in, we can create it and see if it was a sucess
		if (vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS) {
			throw std::runtime_error("failed to create render pass!");
		}
	}

	/*So that images can be displayed on screen, we need to create the graphics pipeline, this will set up all the required details. It sets up all
	the required operations to take the vertices and textures of the meshes to the pixels that will be used as the render targers.*/
	void createGraphicsPipeline() {
		auto vertShaderCode = readFile("shaders/vert.spv"); //load the bytecode of the 2 shaders
		auto fragShaderCode = readFile("shaders/frag.spv");

		VkShaderModule vertShaderModule = createShaderModule(vertShaderCode); //create shader modules for both fragmetns and vertices, only needed during
		VkShaderModule fragShaderModule = createShaderModule(fragShaderCode); //..the creation of the pipeline and can be destroyed after, optimising performance

		VkPipelineShaderStageCreateInfo vertShaderStageInfo = {}; //create a struct to store the vertex shader information
		vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO; //were at the sharer stage
		vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;	//and we will be using it at the vertex stage in the pipeline
		vertShaderStageInfo.module = vertShaderModule;	//this is the module containing the code for the vertex shader that we created earlier
		vertShaderStageInfo.pName = "main"; //we may wish to use multiple frag shaders into a single module to define various behaviours,
											// we are just sticking to the standard main. 1 per each.

		VkPipelineShaderStageCreateInfo fragShaderStageInfo = {}; //similar to above, we need a struct to store fragment shader info
		fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO; //still at the shader stage
		fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;	//and we will be using this one at the fragment stage of the pipeline
		fragShaderStageInfo.module = fragShaderModule;	//the appropriate shader module
		fragShaderStageInfo.pName = "main";	//and using the default main behavior

		//an array to store these 2 structs so that they can be easily referenced later on
		VkPipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

		//structure to describe the format of the vertex data that will be passed through the pipeline, here we are hardcoding so no vertex
		//data to pass through
		VkPipelineVertexInputStateCreateInfo vertexInputInfo = {}; //create the struct
		vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO; //and we are dealing with the vertex input stage
		vertexInputInfo.vertexBindingDescriptionCount = 0;	//both are 0 because we are hardcoding it
		vertexInputInfo.vertexAttributeDescriptionCount = 0;

		//structure to store the info regarding what kind of geometry will be drawn from the provided vertices, and is primitive restard enabled?
		VkPipelineInputAssemblyStateCreateInfo inputAssembly = {}; //create the struct
		inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO; //we are dealing with the assebuly of the input vertivcs
		inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST; //how will we be using them to create geometry? (Check spec for all options)
		inputAssembly.primitiveRestartEnable = VK_FALSE; //we are not going to be using primitive restart

		//create the viewport in which the data will be rendered to
		VkViewport viewport = {};
		viewport.x = 0.0f; //starting positions
		viewport.y = 0.0f;
		viewport.width = (float)swapChainExtent.width; //end positions is the size of the window
		viewport.height = (float)swapChainExtent.height;
		viewport.minDepth = 0.0f;	//min and max value to use for the frame buffer, [0,1]
		viewport.maxDepth = 1.0f;

		//the scissor defines defines which regions of the image will be actually stored in the frame buffer, we want to use the full viewport
		VkRect2D scissor = {};
		scissor.offset = { 0, 0 }; //start at the same position
		scissor.extent = swapChainExtent; //and use the same dimension

		//once the viewport and scissor have been defined, then need to create a viewport struct to store the appropritate info
		VkPipelineViewportStateCreateInfo viewportState = {}; //create the struct
		viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO; //we are storing the viewport state
		viewportState.viewportCount = 1;	//we are only going to be using 1 viewport
		viewportState.pViewports = &viewport;	//and it will be the one we defined
		viewportState.scissorCount = 1;	//same with the scissor
		viewportState.pScissors = &scissor;

		/*Now we need to set up the raster stage of the pipeline, it will eventually take the geometry shaped by the vertices from
		the vertex shader and then turn it into fragments to be coloursed by the fragment shader. Also allows many other options
		to be configured that may aid in performance depending on the application*/
		VkPipelineRasterizationStateCreateInfo rasterizer = {}; //set up the struct to store the info about the stage
		rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO; //were at the raster stage
		rasterizer.depthClampEnable = VK_FALSE; //and we do not want to clamp the near and far planes, we just want to discard them
		rasterizer.rasterizerDiscardEnable = VK_FALSE; //we do not want to skip the stage, we will be wanting to output to the frame buffer
		rasterizer.polygonMode = VK_POLYGON_MODE_FILL;	//we want to fill all the polygones, could also just draw lines or points
		rasterizer.lineWidth = 1.0f;	//thickness of the lines between vertices in terms of number of fragments
		rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;	//we just want to cull the back face
		rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;	//and this is the vertex ordering that we are going to use
		rasterizer.depthBiasEnable = VK_FALSE;	//dont want to add any bias to the fragment slope, but if using shadow maps then may be useful to enable

		/*the next part of the pipleine is to set up multisampling, a type of anti-aliasing so we can get smooth edges. Using this is
		more efficient than rendering a higher resolution and then downsizing*/
		VkPipelineMultisampleStateCreateInfo multisampling = {}; //create the struct
		multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO; //and we are storing info on multisampling
		multisampling.sampleShadingEnable = VK_FALSE;	//we are not going to be using it just yet so disable it
		multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;	//not really used so only take the 1 sample, i.e use identity

		/*The next stage of the pipeline is the colour blending. here it takes the colour returned from the fragment shader and
		it needs to be combinded with the colour already in the framebuffer. We need to set up 2 structs for this, one will deal
		with the configuration per each attatched frame buffer, adn the second deals with the global colour blending settings.
		We are only using one. May wish to use multiple if using one to render an object a specific way and then the second to 
		get it ready to display on screen.*/
		VkPipelineColorBlendAttachmentState colorBlendAttachment = {}; //set up the struct
		colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT; //will be storing rgba bits
		colorBlendAttachment.blendEnable = VK_FALSE;	//but we are not blending as we are just using the one framebuffer, so just pass through the colour from the fragment shader

		/*set up the structure that deals with all the framebuffers and allows to set blend constraints that can be used in the 
		blending calculations*/
		VkPipelineColorBlendStateCreateInfo colorBlending = {};
		colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO; //we are at the colour blending stage
		colorBlending.logicOpEnable = VK_FALSE;	//we arent going to use bitwise blending
		colorBlending.logicOp = VK_LOGIC_OP_COPY;	//if we were then this is where we would store the bitwise operation
		colorBlending.attachmentCount = 1;	//we are just using the 1 blending attatchment
		colorBlending.pAttachments = &colorBlendAttachment;	//and it was the one we specified above
		colorBlending.blendConstants[0] = 0.0f;	//not blending with anything so keep as 0 so nothign will change
		colorBlending.blendConstants[1] = 0.0f;
		colorBlending.blendConstants[2] = 0.0f;
		colorBlending.blendConstants[3] = 0.0f;

		/*I f we want to use uniform values in the shaders then we specify them hwere, this woulf allow use to modify the shdaers
		without having to recomute them, such as matrix transformations. They need to be specified in this struct. We wont use them
		yet but a default struct atleast needs to be set up*/
		VkPipelineLayoutCreateInfo pipelineLayoutInfo = {}; //create the struct
		pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO; //we are storing pipeline layout info
		pipelineLayoutInfo.setLayoutCount = 0; //not using any layouts
		pipelineLayoutInfo.pushConstantRangeCount = 0; //also not using dynamic values in shaders

		//now everything has been set up then we need to set up the pipeline, and check it was created successfully
		if (vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS) {
			throw std::runtime_error("failed to create pipeline layout!");
		}

		/*Now all of the pipeline segments have been created, we can specify the functionality for the actual pipeline*/
		VkGraphicsPipelineCreateInfo pipelineInfo = {}; //create the struct
		pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO; //and we are specifying the pipeline creation info
		pipelineInfo.stageCount = 2;	//we are using 2 stages in the pipeline
		pipelineInfo.pStages = shaderStages; //a reference to them, the vertex and fraagmetn shaders
		pipelineInfo.pVertexInputState = &vertexInputInfo; //this is where we get the information for the vertex inputs
		pipelineInfo.pInputAssemblyState = &inputAssembly; //and how they will be assembled
		pipelineInfo.pViewportState = &viewportState; //what will the viewport be
		pipelineInfo.pRasterizationState = &rasterizer; //how will we be rasterising them
		pipelineInfo.pMultisampleState = &multisampling; //this is where we defined any of the multisampling info (we aren't using it in this application)
		pipelineInfo.pColorBlendState = &colorBlending; //how are we going to be blending the colours
		pipelineInfo.layout = pipelineLayout; //whats the layout of the pipeline going to be, we arent using any additional layouts
		pipelineInfo.renderPass = renderPass;//reference the render pass that we have defined
		pipelineInfo.subpass = 0; //which subpass will the graphics pipeline be used
		pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; //we do not want to derive an aditional pipeline based of this information in this applucation

		//The pipeline is all set up so just needs to be created, check it was done successfully and display an error if it was not
		if (vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline) != VK_SUCCESS) {
			throw std::runtime_error("failed to create graphics pipeline!");
		}

		vkDestroyShaderModule(device, fragShaderModule, nullptr); //now the pipeline is set up we dont need to work with the shaders anymore so destory
		vkDestroyShaderModule(device, vertShaderModule, nullptr);
	}

	/*Function to create the framebuffer that will store all the information to display on the screen by taking data in from the swapchain. It combines the image views and the details
	from the render pass */
	void createFramebuffers() {
		swapChainFramebuffers.resize(swapChainImageViews.size()); //there are going to be the same number of framebuffers as number of image views in the swap chain

		for (size_t i = 0; i < swapChainImageViews.size(); i++) {
			VkImageView attachments[] = { //the attatchments getting passed througgh are just the image views that are going to be used in the framebuffer
				swapChainImageViews[i]
			};

			VkFramebufferCreateInfo framebufferInfo = {}; //create the frame buffer struct
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO; //we are storing framebuffer information
			framebufferInfo.renderPass = renderPass; //the render pass getting used is the one previusly defined
			framebufferInfo.attachmentCount = 1;	//the only attachment for each is just the imageview
			framebufferInfo.pAttachments = attachments;	//the image views are stored in an array
			framebufferInfo.width = swapChainExtent.width;	//we are maintaining the same width and height
			framebufferInfo.height = swapChainExtent.height;
			framebufferInfo.layers = 1;	//the swap chain images are just single images so use only 1 layer

			//create the frame buffer from the required information and check it was created without failure
			if (vkCreateFramebuffer(device, &framebufferInfo, nullptr, &swapChainFramebuffers[i]) != VK_SUCCESS) {
				throw std::runtime_error("failed to create framebuffer!");
			}
		}
	}

	/*Function to create the command ppols used for allocating command buffers*/
	void createCommandPool() {
		QueueFamilyIndices queueFamilyIndices = findQueueFamilies(physicalDevice); //finds aall the queues that can be used on the physical device we are using

		VkCommandPoolCreateInfo poolInfo = {}; //create the command pool struct
		poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO; //storing command pool info
		poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value(); //where abouts is the graphics queue?

		//create the command pool with the above specified info, throw error if fails
		if (vkCreateCommandPool(device, &poolInfo, nullptr, &commandPool) != VK_SUCCESS) {
			throw std::runtime_error("failed to create command pool!");
		}
	}

	/*With the command pool created, we can now create the command buffers to store all of the commands that need to be done for each image in the swap chain*/
	void createCommandBuffers() {
		//the amount of command buffers is going to be the same as the number of images in the swap chain
		commandBuffers.resize(swapChainFramebuffers.size()); 

		//we need to allocate the command buffers
		VkCommandBufferAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO; //storing the command buffer allocation info
		allocInfo.commandPool = commandPool;	//and we will be unsing commands from the command pool
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;//we want to be able to submit the commands to a queue for execution
		allocInfo.commandBufferCount = (uint32_t)commandBuffers.size(); //how many commands are there?

		//with the information filled in, we can allocate the command buffers
		if (vkAllocateCommandBuffers(device, &allocInfo, commandBuffers.data()) != VK_SUCCESS) {
			throw std::runtime_error("failed to allocate command buffers!");
		}

		//with the command buffers allocated, we can record what commands we want in them so we iterate over all the command buffers
		for (size_t i = 0; i < commandBuffers.size(); i++) {
			VkCommandBufferBeginInfo beginInfo = {}; //create a structure for each one
			beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO; //and were storing command buffer information
			//here we can also set flags to set how the command buffers are going to be used depending on the situation

			//create the command buffer for eachimage in swap chain
			if (vkBeginCommandBuffer(commandBuffers[i], &beginInfo) != VK_SUCCESS) {
				throw std::runtime_error("failed to begin recording command buffer!");
			}

			//so that we can draw anything, we need to set up the details for the render pass, we need to configure the struct for it
			VkRenderPassBeginInfo renderPassInfo = {}; //struct to store configuration info
			renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO; //storing render pass details
			renderPassInfo.renderPass = renderPass; //define the redner pass that we shall be using
			renderPassInfo.framebuffer = swapChainFramebuffers[i]; //and the apropriate frame buffer
			renderPassInfo.renderArea.offset = { 0, 0 }; //we arent ofsetting the image by anything
			renderPassInfo.renderArea.extent = swapChainExtent; //and using the same dimensions as the images in the swap chain

			VkClearValue clearColor = { 0.0f, 0.0f, 0.0f, 1.0f }; //the clear colour used for colour attachment, may want to add hues so could do that here
			renderPassInfo.clearValueCount = 1; //only using 1 type of clear colour
			renderPassInfo.pClearValues = &clearColor; //refrence the colour to be used

			//with the set up sorted, can now run the render pass, this will recor all the render commands.
			//takes the command buffer itll be stored in, the respective rendr pass info, and we are only using primary command buffers so inline
			vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

			//now we can bind the pipeline as it has all been set up and vulkan has been toldwhich operations to execute and where to execute them
			vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

			//now draw the output from the pipeline, 3 vertices, 1 instance, no offset
			vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

			//tell that the render pass has finished. Now the all the commands have been recordered to the command buffer
			vkCmdEndRenderPass(commandBuffers[i]);

			//now the command buffer is complete we can tell it nothing else to recorded. if something fails to be recorded then we can throw an error
			if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS) {
				throw std::runtime_error("failed to record command buffer!");
			}
		}
	}

	/*Function to create all the objects we need to allow for syncronization between the cpu-gpu and gpu-gpu*/
	void createSyncObjects() {
		imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT); //we need to have a semaphore for only the frames that are in flight
		renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
		inFlightFences.resize(MAX_FRAMES_IN_FLIGHT); //we only need fences for the frames we are dealing with
		imagesInFlight.resize(swapChainImages.size(), VK_NULL_HANDLE); //needs to be the size of the number of images in the swap chain, as there will be a fence per image

		//Need to generate a struct for each semaphore, they are pretty bare as no use for any other info other than the type
		VkSemaphoreCreateInfo semaphoreInfo = {};
		semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO; //we are detailing semaphore info

		//we need to set up the fence so it is already signalled, the default is to not be
		VkFenceCreateInfo fenceInfo = {};
		fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

		//we need to iterate over all the frames that are lined up to be processed and create both semaphors for them all.
		for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
			if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageAvailableSemaphores[i]) != VK_SUCCESS || //if the creation fails for either then throw an error
				vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderFinishedSemaphores[i]) != VK_SUCCESS ||
				vkCreateFence(device, &fenceInfo, nullptr, &inFlightFences[i]) != VK_SUCCESS) {
				throw std::runtime_error("failed to create synchronization objects for a frame!");
			}
		}
	}

	/*This is the function that will put everything together so that we can have an image to display on the screen. It takes an image from the swap
	chain, executes the appropriate command buffer using that image an attachment for the frame buffer, and then returns the image to the swap chain
	so that it can be displayed*/
	void drawFrame() {
		vkWaitForFences(device, 1, &inFlightFences[currentFrame], VK_TRUE, UINT64_MAX); //need to wait for the current frame to be finished before moving on

		uint32_t imageIndex; //which image are we currently dealing with?
		//assign that image index, and get the next image to be drawn, we can use a timeout to make sure that it doent take too long and waste time
		VkResult result = vkAcquireNextImageKHR(device, swapChain, UINT64_MAX, imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, &imageIndex);

		//we need to see if any previous frame is using the current image, if so we need to wait fo it to finish up
		if (imagesInFlight[imageIndex] != VK_NULL_HANDLE) {
			vkWaitForFences(device, 1, &imagesInFlight[imageIndex], VK_TRUE, UINT64_MAX);
		}
		//once it becomes available, then we indicate thaat this image it getting used by this current frame
		imagesInFlight[imageIndex] = inFlightFences[currentFrame];

		//We need to configue the queue information and the syncornization here
		VkSubmitInfo submitInfo = {}; //create struct for submission info
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

		VkSemaphore waitSemaphores[] = { imageAvailableSemaphores[currentFrame] }; //the semaphore that needs to be waited on until execution
		VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT }; //we want to be writing colours after it is available
		submitInfo.waitSemaphoreCount = 1; //we only need to wait on the one semaphore
		submitInfo.pWaitSemaphores = waitSemaphores; //what is the semaphore we're waiting for?
		submitInfo.pWaitDstStageMask = waitStages; //and what stages are waiting to be processed, but we could do other stages here to improve performance as long as they dont mess with this...

		submitInfo.commandBufferCount = 1; //how many command bufffers are we sending for execution
		submitInfo.pCommandBuffers = &commandBuffers[imageIndex]; //and what are they?

		VkSemaphore signalSemaphores[] = { renderFinishedSemaphores[currentFrame] }; //which semaphores signal we can move on
		submitInfo.signalSemaphoreCount = 1; //and how many do we have to get?
		submitInfo.pSignalSemaphores = signalSemaphores; //and what are they?

		vkResetFences(device, 1, &inFlightFences[currentFrame]);//need to reset the fence so it says it is unsignalled

		//with all the semaphores and fences set up, they can be submitted to the command buffer, if that fails can throw an error, waits until command buffer has finsihed them moves on
		if (vkQueueSubmit(graphicsQueue, 1, &submitInfo, inFlightFences[currentFrame]) != VK_SUCCESS) {
			throw std::runtime_error("failed to submit draw command buffer!");
		}

		//once everything is set up, then the final stage is submitting the image back to the swap chain to be displayed. We sort the presentation of it
		//out here
		VkPresentInfoKHR presentInfo = {}; //create a struct to store presentation information
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

		presentInfo.waitSemaphoreCount = 1; //how many semaphores do we need to wait on before putting in chain to display
		presentInfo.pWaitSemaphores = signalSemaphores; //which are they?

		//need to specify what the swap chains to present the images to
		VkSwapchainKHR swapChains[] = { swapChain }; //what chain are we using?
		presentInfo.swapchainCount = 1; //and how many are we wrting to?
		presentInfo.pSwapchains = swapChains; //reference to them

		presentInfo.pImageIndices = &imageIndex; //what is the index for each image?

		result = vkQueuePresentKHR(presentQueue, &presentInfo); //submit a request to present an image to the swap chain

		currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT; //which frame are we currently working on?
	}

	/*Before the shaders can be passed on to the pipeline, they need to be correctly wrapped in a vulkan object*/
	VkShaderModule createShaderModule(const std::vector<char>& code) {
		VkShaderModuleCreateInfo createInfo = {}; //create the struct to store the appropriate info
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO; //the struct is storing shader details
		createInfo.codeSize = code.size();	//the data is passed as a vector array so the size is the size of the array
		createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());	//the the data is just the data of the array

		VkShaderModule shaderModule; //define the shader model and create it below, if it fails then throw and error
		if (vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule) != VK_SUCCESS) {
			throw std::runtime_error("failed to create shader module!");
		}

		return shaderModule; //return the shader module, just a wrapper around the bytecode but allows a bit of extra info to be stored
	}

	/*These three functions below are implemented to optimise the swap chain, without them it could work but would not be as efficient
	For the swap chain we woul dhave ideal values to set, but sometimes these are not available so we need the logic to find the best
	alternative. That is done here.*/

	/*The surface format stores the ideal colour depth. The input takes all the available format and returns the ideal one*/
	VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
		//we go through all the available formats, and if OUR CHOSEN ideal one exists. then return that
		for (const auto& availableFormat : availableFormats) {
			if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
				return availableFormat;
			}
		}
		//if our chosen ideal is not available, return the first one that exists in the format. This will still work.
		return availableFormats[0];
	}

	/*Similar to above, this chooses what will be displayed and how. In this implementation we are trying to see if triple buffering
	is available as it is a good middleground between latency and not tearing*/
	VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes) {
		//go through all the modes and see if our chosen ideal is available
		for (const auto& availablePresentMode : availablePresentModes) {
			if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
				return availablePresentMode;
			}
		}
		//if our chosen ideal is not available then default to this as it is always included, it is very similar to vsync.
		return VK_PRESENT_MODE_FIFO_KHR;
	}

	/*function to choose what size image to keep in the swap chain, by default the best option is just the window resolution*/
	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) {
		//Check the width and if it is within range then just use window resolution
		if (capabilities.currentExtent.width != UINT32_MAX) {
			return capabilities.currentExtent;
		}
		else { //if not then calculate the framebuffer size
			int width, height;
			glfwGetFramebufferSize(window, &width, &height);

			VkExtent2D actualExtent = {
				static_cast<uint32_t>(width),
				static_cast<uint32_t>(height)
			};
			//make sure that it is clamped in between the min and max supported resolution
			actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
			actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

			return actualExtent;
		}
	}

	/*function to populate the struct storing details about the swapchain*/
	SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device) {
		SwapChainSupportDetails details; //somewhere to store the details of the swapchain

		//first we need to find out what surface capabilities the device has so that we dont ask it to do something it cant, or implement somehting it can already do
		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

		//Second we need to query the surface formats it can use, this will be a list of structs
		uint32_t formatCount; //how many formats are there?
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr); //function call to find out

		if (formatCount != 0) { //as long as it is able to display something
			details.formats.resize(formatCount); //make the size of formats, the number of formats available
			vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data()); //then store the data for each of the formats
		}

		//finally we need to find out the presentation modes available
		uint32_t presentModeCount; //how many are there?
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr); //function call to find out how many there are

		if (presentModeCount != 0) { //similar to above, resize, and then store the data
			details.presentModes.resize(presentModeCount);
			vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
		}

		return details; //we will return all the details of the swapchain
	}

	/*Various cards are suitable for different tasks, this function is used to check whether it is suitable
	for what the programmer wants it to do.*/
	bool isDeviceSuitable(VkPhysicalDevice device) {
		QueueFamilyIndices indices = findQueueFamilies(device); //check to see if the device supports graphics bits

		bool extensionsSupported = checkDeviceExtensionSupport(device);

		//we need to see if the swap chain supported is adequate for our application
		bool swapChainAdequate = false; //init to false so asume it isnt appropriate
		if (extensionsSupported) { //check to see if there are etensions supported on the device
			SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device); //is there swap chain support?
			swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty(); //make sure that there exist display formats and presentation modes
		}

		return indices.isComplete() && extensionsSupported && swapChainAdequate; //if all the checks pass then the device is suitable
	}

	/*Function to check that all extenstions required are supported, such as being able to display on a screen*/
	bool checkDeviceExtensionSupport(VkPhysicalDevice device) {
		uint32_t extensionCount; //how many extensions are supported
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr); //enumerate all the extensions

		std::vector<VkExtensionProperties> availableExtensions(extensionCount); //create a vector to store all the extensions
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data()); //and then store all the data

		std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end()); //set of strings to represent the unconfirmed required extensions, then can just compare to check

		for (const auto& extension : availableExtensions) { //make sure all extensions are a member of the set of extensions, if they are remove it from the set.
			requiredExtensions.erase(extension.extensionName);
		}

		//if all the extensions are present then the list of required extensions that arent included would be empty so return true
		return requiredExtensions.empty();
	}

	/*Function to find all the queue indicies and popualte the structs with them*/
	QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device) {
		QueueFamilyIndices indices; //the indices to store

		uint32_t queueFamilyCount = 0; //initialise the numbre of families to be 0 incase non are available
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr); //update the count with how manyu the device supports

		//this stores some information aboit the familoies including the type of operations that are supported and the number of queues that are supported
		std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount); //create a vector storing all the properties
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

		/*Here we need to find onw queue family that supports graphics bit so that things can be shown on screen*/
		int i = 0;
		for (const auto& queueFamily : queueFamilies) {
			if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				indices.graphicsFamily = i; //if it is suitable increment the value so can be later used to chec to see if device is suitbale
			}

			VkBool32 presentSupport = false; //does it support displaying on screen? init to false
			vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport); //get the device support information

			if (presentSupport) { //if it does support then increment counter
				indices.presentFamily = i;
			}

			if (indices.isComplete()) { //if the device is suitbale then break
				break;
			}

			i++;
		}

		return indices;
	}

	/*Function to return the list of extensions based on whether validation layers are enabled*/
	std::vector<const char*> getRequiredExtensions() {
		uint32_t glfwExtensionCount = 0; //initialise number of extensions to 0 incase non are added
		const char** glfwExtensions;
		/*If you are using glfw to display a window then the glfw specified extensions are always needed*/
		glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
		std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

		/*To handle any debug messages then we can optionally add this.*/
		if (enableValidationLayers) {
			extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
		}

		return extensions; //Once all appropriate extensions have been added to a vector, return it.
	}

	/*function to check if all the requested layers are available*/
	bool checkValidationLayerSupport() {
		uint32_t layerCount;	//number of layer properties available
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);	//enumerates the layer properties and returns the number available

		std::vector<VkLayerProperties> availableLayers(layerCount); //create vector to store layer properties with required size as calcualated above
		vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data()); //store all the available layer properties in created array

		//for each validation layer, check to see if it exists in the available layers
		for (const char* layerName : validationLayers) {
			bool layerFound = false;	//set to not found as default

			/*for each of the available layers, extract the layer name, then compare this
			to the current validation layer, if they are the same then the layer exists in
			the available layers, then set layer found to be true and break so can 
			check the next layer*/
			for (const auto& layerProperties : availableLayers) {
				if (strcmp(layerName, layerProperties.layerName) == 0) {
					layerFound = true;
					break;
				}
			}

			if (!layerFound) { //if any layer from the validationLayers was never found then return false
				return false;
			}
		}

		return true; //once gone through all the validationLayers and all are in the availableLayers, return true
	}

	/*function used to read in the data from the shader files, it will read in all the bytes and store them in a byte array*/
	static std::vector<char> readFile(const std::string& filename) {
		std::ifstream file(filename, std::ios::ate | std::ios::binary); //get the file and start reading at the end of the file, read it as a binary file

		if (!file.is_open()) { //if we were unable to open the file then throw an error
			throw std::runtime_error("failed to open file!");
		}

		size_t fileSize = (size_t)file.tellg(); //we now know the size of the file so can allocate accordingly
		std::vector<char> buffer(fileSize); //allocate a char array the size of the file

		file.seekg(0); //seek back to the start
		file.read(buffer.data(), fileSize); //and then read the file in

		file.close(); //once the file has been read then close it

		return buffer; //and return the array of bytes
	}

	/*A function to display what sort of error you have if in debug mode and youve made an error
	arg1 - severity of the error, used to check if the error is above a certiain threshold so it can be dealt
			with or ignored
	arg2 - tells you something may be up with your program, ie non optimised, violating spec or something else
	arg3 - a call back to a struct containing details of the message
	arg4 - a pointer that was specified during setup of the call so a user can pass their own data*/
	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData) {
		std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl; //display the error message
		return VK_FALSE;
	}
};

/*The main function. This initialises the vulkan functions stored in the class
 and then runs the application.*/
int main() {
	HelloTriangleApplication app; //initialise a vulkan application from the class

	try {
		app.run();	//try to call the run funtion in the application
	}
	catch (const std::exception & e) {	//if there are any errors in the exectution of running the applicaion then get passed back and displayed in the console
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;			//the program ends with a failure
	}

	return EXIT_SUCCESS; //if all goes as planned and there are no errors in the execution then exit with a success
}